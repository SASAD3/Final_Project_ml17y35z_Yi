# Final_Project_ml17y35z_Yi

There are three notebook files(.ipynb) for this entire project, and none of them need extensional packages, just putting in juyter notebook it should be work.

***Advance_strategy_2_vs._Advance_strategy_3.ipynb*** and ***Greedy_selection_vs._Advanced_strategy_2.ipynb*** are two examples of different AI strategies competing each other. 
The playing process will be showed step by step via test outputs, and the Iteration parameter can be changed to run multiple plays and result will be displayed.

***Samurai_Final_soultion.ipynb*** is for human testing, a human player can input the information to play against the AI player(using Advance strategy 4) via test-based interface.
There are some instructions that need to be know before playing:
1.  The depth limitation need to be input to define the Minimax algorithm, and only the odd number is be acceptable. In addition, when the depth input = 1, the algorithm will not have any look-forward which actually is as same as the Advance strategy 3. However when depth input = 3 or larger,the computation time will be extremely long. Normally, 30-120m will be cost for each move of AI player when the depth limitation is 3, but it will continually decrease with the decreasing playable tile and piece. Please input the correct number by following the examples cause there have not too much error input detection due to the time limitation.
2.  Please input the correct number by following the examples cause there have not too much error input detection due to the time limitation.
3.  Please use photo file “**Real_Board.png**” to know coordinate representation of each tiles.
4.  The meaning of **mainlan tile** and **sea tile** matrices in interface are as follow(details in section 3.3.1 in the report):
    * Row 1, 2 = Coordinate: Special (x,y) coordinate for each tile.
    * Row 3 = label: type of this file.
    * Row 4 = Place state: No one takes current tile with value 0; Player1 takes current tile with value 1; Row 3 = Player2 takes current tile with value 2.
    * Row 5 = Rice: The Rice captured points get by a player.
    * Row 6 = Helmet: The Helmet captured points get by a player.
    * Row 7 = Buddha: The Buddha captured points get by a player.
    * Row 8 = Update state: If this tile is placed with a new piece, update state will be 1 to wait for updating; Value 0 means not waiting for update.
    * Row 9 = Tile index: Each tile has an unique index.
5.  The meaning of **figure tile** matrices in interface are as follow(details in section 3.3.1 in the report):
    * Row 1, 2 = Coordinate: Special (x,y) coordinate for each tile.
    * Row 3 = label: type of this file.
    * Row 4 = Player1 Rice:  The Rice captured points for player one on current figure tile.
    * Row 5 = Player1 Helmet:  The Helmet captured points for player one on current figure tile.
    * Row 6 = Player1 Buddha:  The Buddha captured points for player one on current figure tile.
    * Row 7 = Player2 Rice:  The Rice captured points for player one on current figure tile.
    * Row 8 = Player2 Helmet:  The Helmet captured points for player one on current figure tile.
    * Row 9 = Player2 Buddha:  The Buddha captured points for player one on current figure tile.



